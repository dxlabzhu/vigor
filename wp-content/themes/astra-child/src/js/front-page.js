if (document.readyState != 'loading'){
    initScripts();
} else {
    document.addEventListener('DOMContentLoaded', initScripts);
}

function initScripts() {
    const imagePath = localizedUrls.templateUrl + '/assets/images/';
    const lowerPacificHeights = {
        lat: 37.785550,
        lng: -122.444575
    };

    function initMap() {
        const map = new google.maps.Map(document.getElementById("google-maps"), {
            zoom: window.innerWidth > 769 ? 14 : 12,
            center: lowerPacificHeights
        });

        setMarkers(map);
    }

    function setMarkers(map) {
        const markersData = [
            ["Vigor XF in Outer Richmond", 37.7758565,-122.4966214, 3],
            ["Vigor XF in Lower Haight", 37.771474, -122.428688, 2],
            ["Vigor XF in Financial District", 37.794274, -122.400123, 1]
        ];


        for (let i = 0; i < markersData.length; i++) {
            const markerData = markersData[i],
                infowindow = infowindows[i];

            const marker = new google.maps.Marker({
                position: {
                    lat: markerData[1],
                    lng: markerData[2]
                },
                map,
                icon: imagePath + "pin.svg",
                title: markerData[0],
                zIndex: markerData[3]
            });

            marker.addListener("click", function() {
                infowindow.open(map, marker);
                marker.setIcon(imagePath + "pin-selected.svg");
                document.getElementById('google-maps-heading').style.opacity = 0;
            });

            infowindow.addListener("closeclick", function() {
                marker.setIcon(imagePath + "pin.svg");
                document.getElementById('google-maps-heading').style.opacity = 1;
            });
        }
    }

    document.querySelectorAll('.elementor-image-gallery').forEach(function(gallery, i, arr) {
        const galleryLength = gallery.querySelectorAll('.gallery-item').length;

        arr[i].querySelector('.gallery-item:nth-of-type(4)').insertAdjacentHTML('beforeend', '<div class="infowindow-gallery-show-more"><span class="infowindow-gallery-show-more__text">' + (galleryLength - 3) + ' more</span></div>');
    });


    const infowindows = [];

    document.querySelectorAll('.vigorfx-info').forEach(function(gymInfo) {
        infowindows.push(new google.maps.InfoWindow({
            content: gymInfo.innerHTML,
            maxWidth: 435
        }));
    });

    initMap();
}